# somemcached

This is a demo project to demonstrate a simple in-memory caching service like memcached

## Project Structure

1. ***`./core`*** - Contains core elements:
   - `net.tsymbaliuk.examples.somecached.core.model` - basic data and messages entity classes
   - `net.tsymbaliuk.examples.somecached.core.netty` - protocol decoder and encoder and command parser
2. **`./server`** - Contains memcache node server elements:
   - `net.tsymbaliuk.examples.somecached.cache` - cache data structure
   - `net.tsymbaliuk.examples.somecached.server` - server components
   - `net.tsymbaliuk.examples.somecached.service` - cache service interface and implementation to interact with cache
3. **`./gradle`** - contains static code analysis configuration and rules

## Build and Run 

### Build fat jar  
```
./gradlew server:shadowJar
```
Resulting jar located in `./server/build/libs/` 

### Execute server application jar
```
java -jar ./server/build/libs/server-1.1A.jar [port [capacity] ]
```
for current version 1.1A
where:
* port - TCP port to bind server to, default 11211
* capacity - maximum capacity of cache service, default 100
 
### Test app
Use [netcat](https://netcat.sourceforge.io/) tool to send/receive memcache text protocol messages.
`./testDate` contains get.txt and set.txt simple files with corresponding messages.

`nc localhost 11211 < testData/set.txt -v`

`nc localhost 11211 < testData/get.txt -v`

