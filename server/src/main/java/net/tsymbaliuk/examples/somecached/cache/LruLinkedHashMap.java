package net.tsymbaliuk.examples.somecached.cache;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * HashMap with LRU Eviction Policy.
 * @param <T> Type of Keys
 * @param <R> Type of Values
 */
public class LruLinkedHashMap<T, R> extends LinkedHashMap<T, R> {
    private static final float LOAD_FACTOR = 0.75f; // standard load factor
    private static final long serialVersionUID = 6425569087417714707L;
    private final int maxItems;


    public LruLinkedHashMap(final int maxItems) {
        super(maxItems + maxItems >>> 2, LOAD_FACTOR, true); // cap ~= maxItems * 1.25
        this.maxItems = maxItems;
    }

    @Override
    protected boolean removeEldestEntry(final Map.Entry<T, R> eldest) {
        return size() > maxItems;
    }
}
