package net.tsymbaliuk.examples.somecached.cache;

import net.tsymbaliuk.examples.somecached.core.model.MemcacheItem;

import java.util.Collections;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Minimal LRU implementation based on Java native LinkedHashMap.
 */
public class LruCacheImpl implements Cache {

    private static final Logger logger = LoggerFactory.getLogger(LruCacheImpl.class);

    private final Map<String, MemcacheItem> map;

    public LruCacheImpl(final int capacity) {
        this.map = Collections.synchronizedMap(new LruLinkedHashMap<String, MemcacheItem>(capacity));
        logger.info("Created {} of capacity {}", this.getClass().getSimpleName(), capacity);
    }

    @Override
    public MemcacheItem get(final String key) throws CacheException {
        logger.info("Getting item for key {}", key);
        return map.get(key);
    }

    @Override
    public void put(final MemcacheItem item) throws CacheException {
        logger.info("Putting item with key {}", item.getKey());
        map.put(item.getKey().getKey(), item);
    }

    @Override
    public void del(final String key) throws CacheException {
        logger.info("Deleting item with key {}", key);
        map.remove(key);
    }

    @Override
    public boolean exists(final String key) throws CacheException {
        logger.info("Checking an item with key {} exists", key);
        return map.containsKey(key);
    }
}
