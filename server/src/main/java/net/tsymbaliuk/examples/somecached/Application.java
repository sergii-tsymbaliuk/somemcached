package net.tsymbaliuk.examples.somecached;

import net.tsymbaliuk.examples.somecached.cache.Cache;
import net.tsymbaliuk.examples.somecached.cache.LruCacheImpl;
import net.tsymbaliuk.examples.somecached.server.Server;
import net.tsymbaliuk.examples.somecached.service.MemcacheService;
import net.tsymbaliuk.examples.somecached.service.MemcacheServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Main class to start the server application.
 *
 * Command line arguments:
 * java -jar Application.jar [port [capacity]]
 * where:
 * * port - TCP port to bind server to, default 11211
 * * capacity - maximum capacity of cache service, default 100
 */
public final class Application {
    private static final Logger logger = LoggerFactory.getLogger(Application.class);

    private static final int DEFAULT_PORT = 11211;
    private static final int DEFAULT_CAPACITY = 100;

    private Application(){}

    /** Entry point */
    public static void main(String[] args) {
        final int port = args.length > 0 ? Integer.parseInt(args[0]) : DEFAULT_PORT;
        final int capacity = args.length > 1 ? Integer.parseInt(args[1]) : DEFAULT_CAPACITY;

        final Cache cache = new LruCacheImpl(capacity);
        final MemcacheService service = new MemcacheServiceImpl(cache);

        logger.info("Starting server");
        new Server(port, service).run();
        logger.info("Stopped server");
    }
}
