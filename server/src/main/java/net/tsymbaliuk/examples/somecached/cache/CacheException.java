package net.tsymbaliuk.examples.somecached.cache;

public class CacheException extends Exception {
    static final long serialVersionUID = -3387416963174823946L;

    public CacheException() {
        super();
    }

    public CacheException(final String message) {
        super(message);
    }

    public CacheException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public CacheException(final Throwable cause) {
        super(cause);
    }
}
