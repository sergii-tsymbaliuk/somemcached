package net.tsymbaliuk.examples.somecached.service;

import net.tsymbaliuk.examples.somecached.cache.Cache;
import net.tsymbaliuk.examples.somecached.cache.CacheException;
import net.tsymbaliuk.examples.somecached.core.model.MemcacheItem;
import net.tsymbaliuk.examples.somecached.core.model.MemcacheKey;
import net.tsymbaliuk.examples.somecached.core.model.MemcacheRequestMessage;
import net.tsymbaliuk.examples.somecached.core.model.MemcacheResponseCode;
import net.tsymbaliuk.examples.somecached.core.model.MemcacheResponseMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MemcacheServiceImpl implements MemcacheService {
    private static final Logger logger = LoggerFactory.getLogger(MemcacheServiceImpl.class);

    private static final MemcacheResponseMessage NOT_FOUND = MemcacheResponseMessage.builder()
            .setResponseCode(MemcacheResponseCode.NOT_FOUND).build();
    private static final MemcacheResponseMessage NOT_STORED = MemcacheResponseMessage.builder()
            .setResponseCode(MemcacheResponseCode.NOT_STORED).build();

    private final Cache cache;

    public MemcacheServiceImpl(final Cache cache) {
        this.cache = cache;
    }

    @Override
    public MemcacheResponseMessage execCommand(final MemcacheRequestMessage req) {
        logger.info("Received request for execution: {}", req);
        try {
            switch (req.getMemcacheCommand()) {
                case ADD:
                    return add(req);
                case SET:
                    return set(req);
                case REPLACE:
                    return replace(req);
                case GET:
                    return get(req);
                case DELETE:
                    return delete(req);
                case UNKNOWN:
                default:
                    logger.error("Bad memcache command: {}", req);
                    return MemcacheResponseMessage.builder()
                            .setResponseCode(MemcacheResponseCode.CLIENT_ERROR)
                            .setErrorMessage("Bad memcache command: " + req)
                            .build();
            }
        } catch (CacheException e) {
            logger.error("Server error", e);
            return MemcacheResponseMessage.builder()
                    .setResponseCode(MemcacheResponseCode.SERVER_ERROR)
                    .setErrorMessage(String.format("Cache returned exception", e))
                    .build();
        }
    }

    private MemcacheResponseMessage set(final MemcacheRequestMessage req) throws CacheException {
        logger.info("Trying to set item for keys {}", req.getKeys());
        final MemcacheResponseMessage.MemcacheResponseMessageBuilder builder = MemcacheResponseMessage.builder();
        cache.put(new MemcacheItem(reqToKey(req), req.getValue()));
        logger.info("Successfully stored cache item for request {}", req);
        return builder.setResponseCode(MemcacheResponseCode.STORED).build();
    }

    private MemcacheResponseMessage replace(final MemcacheRequestMessage req) throws CacheException {
        logger.info("Trying to replace item for keys {}", req.getKeys());
        if (!cache.exists(req.getKey())) {
            logger.error("Unable to execute request {}, key does non exist", req);
            return NOT_STORED;
        }
        return set(req);
    }

    private MemcacheResponseMessage add(final MemcacheRequestMessage req) throws CacheException {
        logger.info("Trying to add item for keys {}", req.getKeys());
        if (cache.exists(req.getKey())) {
            logger.error("Unable to execute request {}, key already exist", req);
            return NOT_STORED;
        }
        return set(req);
    }

    private MemcacheKey reqToKey(final MemcacheRequestMessage req) {
        return new MemcacheKey(req.getKey(), req.getFlags(), req.getExptime());
    }

    private MemcacheResponseMessage delete(final MemcacheRequestMessage req) throws CacheException {
        logger.info("Trying to get item for keys {}", req.getKeys());
        if (!cache.exists(req.getKey())) {
            logger.error("Cannot execute delete request, key does not exist {}", req);
            return NOT_FOUND;
        }
        cache.del(req.getKey());
        logger.info("Delete request completed {}", req);
        return MemcacheResponseMessage.builder().setResponseCode(MemcacheResponseCode.DELETED).build();
    }

    private MemcacheResponseMessage get(final MemcacheRequestMessage req) throws CacheException {
        logger.info("Trying to get item for keys {}", req.getKeys());
        final MemcacheResponseMessage.MemcacheResponseMessageBuilder builder = MemcacheResponseMessage
                .builder()
                .setResponseCode(MemcacheResponseCode.VALUE);
        for (final String key : req.getKeys()) {
            if(!cache.exists(key)) {
                logger.error("Unable get item, key {} does not exists for request {}", key, req);
                return MemcacheResponseMessage.builder()
                        .setResponseCode(MemcacheResponseCode.NOT_FOUND)
                        .setErrorMessage(String.format("Cache does nto contain key %s", key))
                        .build();
            }
            builder.addValue(cache.get(key));
        }
        logger.info("Successfully get all items per request {}", req);
        return builder.build();
    }
}
