package net.tsymbaliuk.examples.somecached.cache;

import net.tsymbaliuk.examples.somecached.core.model.MemcacheItem;

public interface Cache {
    MemcacheItem get(final String key) throws CacheException;

    void put(final MemcacheItem item) throws CacheException;

    void del(final String key) throws CacheException;

    boolean exists(final String key) throws CacheException;
}
