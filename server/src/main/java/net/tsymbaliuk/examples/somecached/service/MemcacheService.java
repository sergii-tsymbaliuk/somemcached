package net.tsymbaliuk.examples.somecached.service;

import net.tsymbaliuk.examples.somecached.core.model.MemcacheRequestMessage;
import net.tsymbaliuk.examples.somecached.core.model.MemcacheResponseMessage;

public interface MemcacheService {
    MemcacheResponseMessage execCommand(final MemcacheRequestMessage req);
}
