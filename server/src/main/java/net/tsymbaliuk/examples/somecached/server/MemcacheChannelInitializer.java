package net.tsymbaliuk.examples.somecached.server;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.string.StringEncoder;
import net.tsymbaliuk.examples.somecached.core.netty.ExceptionInboundHandler;
import net.tsymbaliuk.examples.somecached.core.netty.MemcacheRequestDecoder;
import net.tsymbaliuk.examples.somecached.core.netty.MemcacheRequestMessageParser;
import net.tsymbaliuk.examples.somecached.core.netty.MemcacheResponseEncoder;
import net.tsymbaliuk.examples.somecached.service.MemcacheService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MemcacheChannelInitializer extends ChannelInitializer<SocketChannel> {

    private static final Logger logger = LoggerFactory.getLogger(MemcacheChannelInitializer.class);
    private final MemcacheService service;

    public MemcacheChannelInitializer(MemcacheService service) {
        super();
        this.service = service;
        logger.info("Created channel handler initializer");
    }

    @Override
    protected void initChannel(SocketChannel ch) throws Exception {
        ch.pipeline().addLast(
                new MemcacheRequestDecoder(new MemcacheRequestMessageParser()),
                new MemcacheServerHandler(service),
                new MemcacheResponseEncoder(),
                new ExceptionInboundHandler(),
                new StringEncoder());
        logger.info("Channel handlers initialized");
    }
}
