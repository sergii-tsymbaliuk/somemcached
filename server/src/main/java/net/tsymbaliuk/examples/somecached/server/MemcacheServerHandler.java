package net.tsymbaliuk.examples.somecached.server;

import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import net.tsymbaliuk.examples.somecached.core.model.MemcacheRequestMessage;
import net.tsymbaliuk.examples.somecached.core.model.MemcacheResponseMessage;
import net.tsymbaliuk.examples.somecached.service.MemcacheService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Main server handler to direct received request to cacheService and send back it's responses.
 */
public class MemcacheServerHandler extends SimpleChannelInboundHandler<MemcacheRequestMessage> {
    private static final Logger logger = LoggerFactory.getLogger(MemcacheServerHandler.class);

    private final MemcacheService service;

    public MemcacheServerHandler(final MemcacheService service) {
        super();
        this.service = service;
    }

    @Override
    protected void channelRead0(final ChannelHandlerContext ctx, final MemcacheRequestMessage msg) throws Exception {
        logger.info("Received Memcache Request {}", msg);
        final MemcacheResponseMessage response = service.execCommand(msg);
        logger.info("Sending Memcache Response {}", response);
        final ChannelFuture future = ctx.channel().writeAndFlush(response);
        future.addListener(ChannelFutureListener.CLOSE);
        logger.info("Completed");
    }
}
