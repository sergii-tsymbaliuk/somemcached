package net.tsymbaliuk.examples.somecached.server;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import net.tsymbaliuk.examples.somecached.service.MemcacheService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The main server class, put together netty protocol decoder, encoder and handlers with cache service implementation.
 */
public class Server {
    private static final Logger logger = LoggerFactory.getLogger(Server.class);

    private final int port;
    private final MemcacheService service;

    /**
     * Constructor
     * @param port The IP port to listen on.
     * @param service {@link MemcacheService} caching service implementation
     */
    public Server(final int port, final MemcacheService service) {
        this.port = port;
        this.service = service;
    }

    /**
     * Server entry point.
     *
     * Bootstrapping netty server, wire together decoder, encoder and handler.
     */
    public void run() {
        logger.info("Starting server ...");
        final EventLoopGroup bossGroup = new NioEventLoopGroup(1);
        final EventLoopGroup workerGroup = new NioEventLoopGroup();
        try {
            final ServerBootstrap serverBootstrap = new ServerBootstrap();
            serverBootstrap.group(bossGroup, workerGroup)
                    .channel(NioServerSocketChannel.class)
                    .childHandler(new MemcacheChannelInitializer(service));

            logger.info("Binding server on port {}", port);
            final ChannelFuture channelFuture = serverBootstrap.bind(port).sync();
            channelFuture.channel().closeFuture().sync();
            logger.info("Server stopped");
        } catch (InterruptedException e) {
            logger.error("Unexpected exception", e);
        } finally {
            logger.info("Shutting down worker groups ...");
            workerGroup.shutdownGracefully();
            bossGroup.shutdownGracefully();
            logger.info("Worker groups were shut down");
        }
    }
}
