package net.tsymbaliuk.examples.somecached;

import java.io.IOException;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class ApplicationTest {

    private static final Logger logger = LoggerFactory.getLogger(ApplicationTest.class);

    private static String[] appArgs = {};
    private ExecutorService executorService;

    @Before
    public void setUp() {
        executorService = Executors.newFixedThreadPool(1);
    }

    @After
    public void tearDown() {
        executorService.shutdown();
    }

    @Test
    public void testAppStartsListenOnPort() throws IOException, InterruptedException {
        executorService.submit(() -> Application.main(appArgs));

        // wait for server to start
        for (int i = 0; i < 5; i++) {
            if (checkPort("localhost", 11211)) {
                break;
            }
            logger.info("Waiting for app to start ...");
            Thread.currentThread().sleep(200);
        }

        assertThat(checkPort("localhost", 11211), is(true));
    }

    private boolean checkPort(final String host, final int port) throws IOException {
        Socket socket = null;
        try {
            socket = new Socket(host, port);
            return true;
        } catch (Exception e) {
            logger.info("Exception occurred " + e);
            return false;
        } finally {
            if (socket != null) {
                socket.close();
            }
        }
    }
}