package net.tsymbaliuk.examples.somecached.service;

import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Map;
import net.tsymbaliuk.examples.somecached.cache.Cache;
import net.tsymbaliuk.examples.somecached.cache.CacheException;
import net.tsymbaliuk.examples.somecached.cache.LruCacheImpl;
import net.tsymbaliuk.examples.somecached.core.model.MemcacheCommand;
import net.tsymbaliuk.examples.somecached.core.model.MemcacheItem;
import net.tsymbaliuk.examples.somecached.core.model.MemcacheKey;
import net.tsymbaliuk.examples.somecached.core.model.MemcacheRequestMessage;
import net.tsymbaliuk.examples.somecached.core.model.MemcacheResponseCode;
import net.tsymbaliuk.examples.somecached.core.model.MemcacheResponseMessage;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.core.Is.is;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class MemcacheServiceImplTest {

    private Cache cache;
    private Cache spyCache;
    private MemcacheService service;

    @Before
    public void setup() {
        cache = new LruCacheImpl(10);
        spyCache = spy(cache);
        service = new MemcacheServiceImpl(spyCache);
    }

    @Test
    public void execCommand_add_new() throws CacheException {
        final MemcacheRequestMessage req = MemcacheRequestMessage
                .builder()
                .setCommand(MemcacheCommand.ADD)
                .addKey("some key")
                .setValue("testvalue".getBytes(StandardCharsets.US_ASCII))
                .build();

        final MemcacheResponseMessage resp = service.execCommand(req);
        final ArgumentCaptor<MemcacheItem> captor = ArgumentCaptor.forClass(MemcacheItem.class);
        verify(spyCache).put(captor.capture());
        final MemcacheItem item = captor.getValue();
        assertThat(item.getKey().getKey(), is("some key"));
        assertThat(item.getData(), is("testvalue".getBytes(StandardCharsets.US_ASCII)));

        assertThat(resp.getResponseCode(), is(MemcacheResponseCode.STORED));
    }

    @Test
    public void execCommand_add_existent() throws CacheException {
        cache.put(new MemcacheItem(
                new MemcacheKey("some key", 0,
                        Instant.now().plus(1, ChronoUnit.HOURS).toEpochMilli()),
                "old value".getBytes(StandardCharsets.US_ASCII)));
        final MemcacheRequestMessage req = MemcacheRequestMessage
                .builder()
                .setCommand(MemcacheCommand.ADD)
                .addKey("some key")
                .setValue("testvalue".getBytes(StandardCharsets.US_ASCII))
                .build();

        final MemcacheResponseMessage resp = service.execCommand(req);
        verify(spyCache, times(0)).put(any(MemcacheItem.class));

        assertThat(resp.getResponseCode(), is(MemcacheResponseCode.NOT_STORED));
    }

    @Test
    public void execCommand_set() throws CacheException {
        cache.put(new MemcacheItem(
                new MemcacheKey("some key", 0,
                        Instant.now().plus(1, ChronoUnit.HOURS).toEpochMilli()),
                "old value".getBytes(StandardCharsets.US_ASCII)));

        final MemcacheRequestMessage req = MemcacheRequestMessage
                .builder()
                .setCommand(MemcacheCommand.SET)
                .addKey("some key")
                .setValue("testvalue".getBytes(StandardCharsets.US_ASCII))
                .build();

        final MemcacheResponseMessage resp = service.execCommand(req);
        final ArgumentCaptor<MemcacheItem> captor = ArgumentCaptor.forClass(MemcacheItem.class);
        verify(spyCache).put(captor.capture());
        final MemcacheItem item = captor.getValue();
        assertThat(item.getKey().getKey(), is("some key"));
        assertThat(item.getData(), is("testvalue".getBytes(StandardCharsets.US_ASCII)));

        assertThat(resp.getResponseCode(), is(MemcacheResponseCode.STORED));
    }

    @Test
    public void execCommand_replace_doesNotExist() throws CacheException {
        final MemcacheRequestMessage req = MemcacheRequestMessage
                .builder()
                .setCommand(MemcacheCommand.REPLACE)
                .addKey("some key")
                .setValue("testvalue".getBytes(StandardCharsets.US_ASCII))
                .build();

        final MemcacheResponseMessage resp = service.execCommand(req);
        verify(spyCache, times(1)).exists(eq("some key"));
        verify(spyCache, times(0)).put(any(MemcacheItem.class));

        assertThat(resp.getResponseCode(), is(MemcacheResponseCode.NOT_STORED));
    }

    @Test
    public void execCommand_delete() throws CacheException {
        final MemcacheRequestMessage req = MemcacheRequestMessage
                .builder()
                .setCommand(MemcacheCommand.DELETE)
                .addKey("some key")
                .build();

        cache.put(new MemcacheItem(
                new MemcacheKey("some key", 0,
                        Instant.now().plus(1, ChronoUnit.HOURS).toEpochMilli()),
                "old value".getBytes(StandardCharsets.US_ASCII)));

        final MemcacheResponseMessage resp = service.execCommand(req);
        verify(spyCache).del(eq("some key"));

        assertThat(resp.getResponseCode(), is(MemcacheResponseCode.DELETED));
    }

    @Test
    public void execCommand_get() throws CacheException {
        cache.put(new MemcacheItem(
                new MemcacheKey("some key 1", 0,
                        Instant.now().plus(1, ChronoUnit.HOURS).toEpochMilli()),
                "item 1 value".getBytes(StandardCharsets.US_ASCII)));

        cache.put(new MemcacheItem(
                new MemcacheKey("some key 2", 0,
                        Instant.now().plus(1, ChronoUnit.HOURS).toEpochMilli()),
                "item 2 value".getBytes(StandardCharsets.US_ASCII)));

        final MemcacheRequestMessage req = MemcacheRequestMessage
                .builder()
                .setCommand(MemcacheCommand.GET)
                .addKey("some key 1")
                .addKey("some key 2")
                .build();

        final MemcacheResponseMessage resp = service.execCommand(req);

        verify(spyCache).get("some key 1");
        verify(spyCache).get("some key 2");

        assertThat(resp.getResponseCode(), is(MemcacheResponseCode.VALUE));

        final Map<MemcacheKey, byte[]> data = resp.getData();
        assertThat(data.size(), is(2));
        assertThat(data.values(),
                containsInAnyOrder("item 1 value".getBytes(StandardCharsets.US_ASCII),
                        "item 2 value".getBytes(StandardCharsets.US_ASCII)));

    }

    @Test
    public void execCommand_unknown() throws CacheException {
        final MemcacheRequestMessage req = MemcacheRequestMessage
                .builder()
                .setCommand(MemcacheCommand.UNKNOWN)
                .build();

        final MemcacheResponseMessage resp = service.execCommand(req);

        assertThat(resp.getResponseCode(), is(MemcacheResponseCode.CLIENT_ERROR));
    }
}