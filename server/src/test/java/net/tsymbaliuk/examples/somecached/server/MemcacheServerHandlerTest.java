package net.tsymbaliuk.examples.somecached.server;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.embedded.EmbeddedChannel;
import java.nio.charset.StandardCharsets;
import net.tsymbaliuk.examples.somecached.cache.CacheException;
import net.tsymbaliuk.examples.somecached.cache.LruCacheImpl;
import net.tsymbaliuk.examples.somecached.core.model.MemcacheItem;
import net.tsymbaliuk.examples.somecached.core.model.MemcacheKey;
import net.tsymbaliuk.examples.somecached.core.netty.MemcacheRequestDecoder;
import net.tsymbaliuk.examples.somecached.core.netty.MemcacheRequestMessageParser;
import net.tsymbaliuk.examples.somecached.core.netty.MemcacheResponseEncoder;
import net.tsymbaliuk.examples.somecached.service.MemcacheServiceImpl;
import org.junit.Before;
import org.junit.Test;

import static java.nio.charset.StandardCharsets.US_ASCII;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;

public class MemcacheServerHandlerTest {

    private LruCacheImpl cache;
    private EmbeddedChannel channel;

    @Before
    public void setUp() {
        cache = new LruCacheImpl(10);
        channel = new EmbeddedChannel();
        channel.pipeline().addLast(new MemcacheRequestDecoder(new MemcacheRequestMessageParser()));
        channel.pipeline().addLast(new MemcacheResponseEncoder());
        channel.pipeline().addLast(new MemcacheServerHandler(new MemcacheServiceImpl(cache)));
    }

    @Test
    public void testParseInboundMessage_set() throws CacheException {
        final ByteBuf buf = Unpooled.copiedBuffer("set xyzkey 1 2 6\r\nabcdef\r\n", StandardCharsets.UTF_8);
        channel.writeInbound(buf);
        final Object o = channel.readOutbound();
        assertThat(o, instanceOf(ByteBuf.class));
        final ByteBuf outboundChannelResponse = (ByteBuf) o;
        final byte[] bytes = new byte[outboundChannelResponse.readableBytes()];
        outboundChannelResponse.readBytes(bytes);
        final String out = new String(bytes, US_ASCII);
        assertThat(out, is("STORED\r\n"));
        assertThat(cache.exists("xyzkey"), is(true));
        final MemcacheItem item = cache.get("xyzkey");
        assertThat(item.getData(), is("abcdef".getBytes(US_ASCII)));
        assertThat(item.getKey().getExptime(), is(2L));
        assertThat(item.getKey().getFlags(), is(1L));
    }

    @Test
    public void testParseInboundMessage_getOne() throws CacheException {
        final MemcacheItem item = new MemcacheItem(
                new MemcacheKey("some-key-1", 3, 34523),
                "test_data_1".getBytes(US_ASCII));
        cache.put(item);
        final ByteBuf buf = Unpooled.copiedBuffer("get some-key-1\r\n", US_ASCII);
        channel.writeInbound(buf);
        final Object o = channel.readOutbound();
        assertThat(o, instanceOf(ByteBuf.class));
        final ByteBuf outboundChannelResponse = (ByteBuf) o;
        final byte[] bytes = new byte[outboundChannelResponse.readableBytes()];
        outboundChannelResponse.readBytes(bytes);
        final String out = new String(bytes, US_ASCII);
        assertThat(out, is("VALUE some-key-1 3 11\r\ntest_data_1\r\nEND\r\n"));
    }

    @Test
    public void testParseInboundMessage_getMany() throws CacheException {
        cache.put(new MemcacheItem(
                new MemcacheKey("some-key-1", 3, 34523),
                "test_data_1".getBytes(US_ASCII)));
        cache.put(new MemcacheItem(
                new MemcacheKey("some-key-2", 4, 34524),
                "test_data_2".getBytes(US_ASCII)));
        cache.put(new MemcacheItem(
                new MemcacheKey("some-key-3", 5, 34525),
                "test_data_3".getBytes(US_ASCII)));
        final ByteBuf buf = Unpooled.copiedBuffer("get some-key-1 some-key-3 some-key-2\r\n", US_ASCII);
        channel.writeInbound(buf);
        final Object o = channel.readOutbound();
        assertThat(o, instanceOf(ByteBuf.class));
        final ByteBuf outboundChannelResponse = (ByteBuf) o;
        final byte[] bytes = new byte[outboundChannelResponse.readableBytes()];
        outboundChannelResponse.readBytes(bytes);
        final String out = new String(bytes, US_ASCII);
        assertThat(out, is(
                "VALUE some-key-1 3 11\r\ntest_data_1\r\n"
                + "VALUE some-key-3 5 11\r\ntest_data_3\r\n"
                + "VALUE some-key-2 4 11\r\ntest_data_2\r\n"
                + "END\r\n"));
    }
}