package net.tsymbaliuk.examples.somecached.cache;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.collection.IsMapContaining.hasKey;

public class LruLinkedHashMapTest {

    private LruLinkedHashMap<String, String> map;

    @Before
    public void setUp() {
        map = new LruLinkedHashMap(3);
    }


    @Test
    public void removeEldestEntry() {
        for (int i = 0; i < 4; i++) {
            map.put("Attempt_" + i, "value: " + i);
        }

        assertThat(map, not(hasKey("Attempt_0")));
        assertThat(map, hasKey("Attempt_1"));
        assertThat(map, hasKey("Attempt_2"));
        assertThat(map, hasKey("Attempt_3"));

        assertThat(map.size(), is(3));
    }
}