package net.tsymbaliuk.examples.somecached.core.model;

import java.util.HashMap;
import java.util.Map;

/**
 * Simple Memcache text protocol response message entity class.
 */
public class MemcacheResponseMessage {
    private final MemcacheResponseCode responseCode;
    private final String errorMessage; // optional
    private final Map<MemcacheKey, byte[]> data; //optional

    protected MemcacheResponseMessage(final MemcacheResponseMessageBuilderImpl builder) {
        this.responseCode = builder.responseCode;
        this.errorMessage = builder.error_message;
        if (!builder.data.isEmpty()) {
            this.data = builder.data;
        } else {
            data = Map.of();
        }
    }

    public MemcacheResponseCode getResponseCode() {
        return responseCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public Map<MemcacheKey, byte[]> getData() {
        return data;
    }

    public boolean hasErrorMessage() {
        return this.errorMessage != null && ! this.errorMessage.isEmpty();
    }

    public boolean hasData() {
        return this.data != null && this.data.size() > 0;
    }

    public static MemcacheResponseMessageBuilder builder() {
        return new MemcacheResponseMessageBuilderImpl();
    }

    private static class MemcacheResponseMessageBuilderImpl implements MemcacheResponseMessageBuilder {
        private MemcacheResponseCode responseCode = MemcacheResponseCode.ERROR;
        private String error_message;
        private final Map<MemcacheKey, byte[]> data = new HashMap<>();

        @Override
        public MemcacheResponseMessageBuilderImpl setResponseCode(final MemcacheResponseCode responseCode) {
            this.responseCode = responseCode;
            return this;
        }

        @Override
        public MemcacheResponseMessageBuilder setErrorMessage(final String errorMessage) {
            this.error_message = errorMessage;
            return this;
        }

        @Override
        public MemcacheResponseMessageBuilder addValue(final MemcacheItem item) {
            data.put(item.getKey(), item.getData());
            return this;
        }

        @Override
        public MemcacheResponseMessageBuilder addAllValues(final Iterable<MemcacheItem> items) {
            items.forEach(this::addValue);
            return this;
        }

        @Override
        public MemcacheResponseMessage build() {
            return new MemcacheResponseMessage(this);
        }
    }

    public interface MemcacheResponseMessageBuilder {

        MemcacheResponseMessageBuilder setResponseCode(final MemcacheResponseCode responseCode);

        MemcacheResponseMessageBuilder setErrorMessage(final String errorMessage);

        MemcacheResponseMessageBuilder addValue(final MemcacheItem item);

        MemcacheResponseMessageBuilder addAllValues(final Iterable<MemcacheItem> items);

        MemcacheResponseMessage build();
    }
}
