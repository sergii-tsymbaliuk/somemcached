package net.tsymbaliuk.examples.somecached.core.model;

import java.util.Objects;

/**
 * Entity object to store cached items key togather with flags and expiration time.
 */
public class MemcacheKey {
    private final String key;
    private final long flags;
    private final long exptime;

    /**
     * Default constructor.
     * @param key String item key
     * @param flags long flags
     * @param exptime long expiration time
     */
    public MemcacheKey(final String key, final long flags, final long exptime) {
        this.key = key;
        this.flags = flags;
        this.exptime = exptime;
    }

    @Override
    public boolean equals(final Object other) {
        if (this == other) {
            return true;
        }
        if (other == null || getClass() != other.getClass()) {
            return false;
        }
        final MemcacheKey that = (MemcacheKey) other;
        return key.equals(that.key);
    }

    @Override
    public int hashCode() {
        return Objects.hash(key);
    }

    public String getKey() {
        return key;
    }

    public long getFlags() {
        return flags;
    }

    public long getExptime() {
        return exptime;
    }
}
