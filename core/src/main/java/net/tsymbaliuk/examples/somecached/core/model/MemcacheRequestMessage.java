package net.tsymbaliuk.examples.somecached.core.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Simple Memcache text protocol request message entity class.
 */
public class MemcacheRequestMessage {
    private final MemcacheCommand memcacheCommand;
    // Array to simplify code and generalize for commands with multiple keys e.g. get key[, key ...],
    private final String[] keys;
    private final long flags;
    // expiration time in seconds. 0 means no delay.
    // If exptime is more than 30 days, it is treated as a UNIX timestamp for expiration.
    private final long exptime;
    private final boolean noreply;
    // payload length
    private final int bytes;
    private final byte[] value;

    private MemcacheRequestMessage(final MemcacheRequestMessageBuilderImpl builder) {
        memcacheCommand = builder.memcacheCommand;
        keys = builder.keys.toArray(String[]::new);
        flags = builder.flags;
        exptime = builder.exptime;
        noreply = builder.noreply;
        value = builder.value;
        bytes = value.length;
    }

    public static MemcacheRequestMessageBuilder builder() {
        return new MemcacheRequestMessageBuilderImpl();
    }

    public MemcacheCommand getMemcacheCommand() {
        return memcacheCommand;
    }

    public String getKey() {
        return keys[0];
    }

    public List<String> getKeys() {
        return Arrays.asList(keys);
    }

    public long getFlags() {
        return flags;
    }

    public long getExptime() {
        return exptime;
    }

    public boolean isNoreply() {
        return noreply;
    }

    public int getBytes() {
        return bytes;
    }

    public byte[] getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "MemcacheRequestMessage{"
                + "memcacheCommand=" + memcacheCommand
                + ", keys=" + Arrays.toString(keys)
                + ", flags=" + flags
                + ", exptime=" + exptime
                + ", noreply=" + noreply
                + ", bytes=" + bytes
                + ", value=" + Arrays.toString(value)
                + '}';
    }

    public interface MemcacheRequestMessageBuilder {

        MemcacheRequestMessageBuilder setCommand(final MemcacheCommand command);

        MemcacheRequestMessageBuilder addKey(final String keys);

        MemcacheRequestMessageBuilder setFlags(final long flags);

        MemcacheRequestMessageBuilder setExpTime(final long exptime);

        MemcacheRequestMessageBuilder setNoReply(final boolean noreply);

        MemcacheRequestMessageBuilder setBytes(int bytes);

        MemcacheRequestMessageBuilder setValue(final byte[] value);

        MemcacheCommand getCommand();

        List<String> getKeys();

        long getFlags();

        long getExpTime();

        boolean isNoReply();

        int getBytes();

        byte[] getValue();

        MemcacheRequestMessage build();
    }

    private static class MemcacheRequestMessageBuilderImpl implements MemcacheRequestMessageBuilder {
        private MemcacheCommand memcacheCommand = MemcacheCommand.UNKNOWN;
        private final List<String> keys = new ArrayList<>();
        private long flags;
        private long exptime;
        private boolean noreply;
        private int bytes;
        private byte[] value = {};


        @Override
        public MemcacheRequestMessageBuilder setCommand(final MemcacheCommand memcacheCommand) {
            this.memcacheCommand = memcacheCommand;
            return this;
        }

        @Override
        public MemcacheRequestMessageBuilder addKey(final String key) {
            this.keys.add(key);
            return this;
        }

        @Override
        public MemcacheRequestMessageBuilder setFlags(final long flags) {
            this.flags = flags;
            return this;
        }

        @Override
        public MemcacheRequestMessageBuilder setExpTime(final long exptime) {
            this.exptime = exptime;
            return this;
        }

        @Override
        public MemcacheRequestMessageBuilder setNoReply(final boolean noreply) {
            this.noreply = noreply;
            return this;
        }

        @Override
        public MemcacheRequestMessageBuilder setBytes(final int bytes) {
            this.bytes = bytes;
            return this;
        }

        @Override
        public MemcacheRequestMessageBuilder setValue(final byte[] value) {
            this.value = Arrays.copyOf(value, value.length);
            return this;
        }

        @Override
        public MemcacheCommand getCommand() {
            return memcacheCommand;
        }

        @Override
        public List<String> getKeys() {
            return keys;
        }

        @Override
        public long getFlags() {
            return flags;
        }

        @Override
        public long getExpTime() {
            return exptime;
        }

        @Override
        public boolean isNoReply() {
            return noreply;
        }

        @Override
        public int getBytes() {
            return bytes;
        }

        @Override
        public byte[] getValue() {
            return value;
        }

        @Override
        public MemcacheRequestMessage build() {
            return new MemcacheRequestMessage(this);
        }
    }
}
