package net.tsymbaliuk.examples.somecached.core.netty;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ReplayingDecoder;
import io.netty.util.internal.AppendableCharSequence;
import net.tsymbaliuk.examples.somecached.core.model.MemcacheCommand;
import net.tsymbaliuk.examples.somecached.core.model.MemcacheRequestMessage;

import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Decoder for subset of Memecahced text protocol.
 *
 * <p>Brief Memcacehd text protocol info:
 * <ul>
 *     <li>Storage commands: set, add, replace, append, prepend, cas</li>
 *     <li>command key [flags] [exptime] length [noreply]</li>
 *     <li>Retrieval commands</li>
 *     <li>get key1 [key2 .... keyn]</li>
 *     <li>gets key1 [key2 ... keyn]</li>
 *     <li>delete key [time] [noreply]</li>
 *     <li></li>
 * </ul>
 * 
 * <p>Command Formats:
 * <ul>
 * <li>set  set key flags exptime length, set key flags exptime length noreply</li>
 * <li>add  add key flags exptime length, add key flags exptime length noreply</li>
 * <li>replace  replace key flags exptime length, replace key flags exptime length noreply</li>
 * <li>cas  cas key flags exptime length casunique, cas key flags exptime length casunique noreply</li>
 * <li>get  get key1 [key2 ... keyn]
 * <li>gets</li>
 * <li>delete  delete key, delete key noreply, delete key expiry, delete key expiry noreply</li>
 * </ul>
 */
public class MemcacheRequestDecoder extends ReplayingDecoder<MemcacheRequestDecoder.DecodingState> {
    private static final Logger logger = LoggerFactory.getLogger(MemcacheRequestDecoder.class);

    public static final int MAX_CONTENT_SIZE = 1048576;

    private static final int INITIAL_CMD_LENGTH = 128;

    private MemcacheRequestMessage.MemcacheRequestMessageBuilder messageBuilder;
    private final MemcacheRequestMessageParser parser;

    /**
     * Default constructor.
     * @param parser instance of {@link MemcacheRequestMessageParser} to parse command parts.
     */
    public MemcacheRequestDecoder(final MemcacheRequestMessageParser parser) {
        super(DecodingState.INITIAL);
        this.parser = parser;
    }

    @Override
    protected void decode(final ChannelHandlerContext ctx, final ByteBuf byteBuf,
                          final List<Object> out) throws MemcacheCommandParsingException {
        logger.info("Received incoming message to decode, state={}", state());
        switch (state()) {
            case INITIAL:
            case COMMAND:
                decodeCommand(byteBuf);
                if (isCommandWithValue(messageBuilder.getCommand())) {
                    checkpoint(DecodingState.VALUE);
                } else {
                    out.add(messageBuilder.build());
                    checkpoint(DecodingState.INITIAL);
                }
                break;
            case VALUE:
                decodeValues(byteBuf);
                out.add(messageBuilder.build());
                checkpoint(DecodingState.INITIAL);
                break;
            default:
                throw new IllegalArgumentException("Unexpected state");
        }
    }

    /** Reads and parses command part of the message from nio buffer. */
    private void decodeCommand(final ByteBuf byteBuf) throws MemcacheCommandParsingException {
        messageBuilder = MemcacheRequestMessage.builder();
        final AppendableCharSequence seq = new AppendableCharSequence(INITIAL_CMD_LENGTH);

        for (;;) {
            char ch = (char) (0xFF & byteBuf.readByte());
            if (ch == Config.LF) {
                if (seq.charAt(seq.length() - 1) == Config.CR) {
                    seq.setLength(seq.length() - 1);
                }
                break;
            } else {
                seq.append(ch);
                if (seq.length() > MAX_CONTENT_SIZE) {
                    throw new MemcacheCommandParsingException("Content too long");
                }
            }
        }
        parser.parseCommand(messageBuilder, seq.toString());
    }

    private boolean isCommandWithValue(final MemcacheCommand command) {
        return command == MemcacheCommand.SET
                || command == MemcacheCommand.ADD
                || command == MemcacheCommand.REPLACE;
    }

    /** Reads data block from nio buffer. */
    private void decodeValues(final ByteBuf byteBuf) {
        final int bytes = messageBuilder.getBytes();
        if (bytes > 0) {
            byte[] buf = new byte[bytes];
            byteBuf.readBytes(buf, 0, bytes);
            messageBuilder.setValue(buf);
        }
        byteBuf.skipBytes(2);
    }

    public enum DecodingState {
        INITIAL,
        COMMAND,
        VALUE
    }
}
