package net.tsymbaliuk.examples.somecached.core.model;

import java.time.Instant;

/**
 * Data objects representing cache item.
 *
 * Stores data {@link MemcacheKey}
 */
public class MemcacheItem {
    private final MemcacheKey key;
    private byte [] data;
    private Instant leastAccessTime;

    public MemcacheItem(final MemcacheKey key) {
        this.key = key;
        this.leastAccessTime = Instant.now();
    }

    public MemcacheItem(final MemcacheKey key, final byte[] data) {
        this(key);
        this.data = data;
    }

    public MemcacheKey getKey() {
        return key;
    }

    public byte[] getData() {
        return data;
    }

    public Instant getLeastAccessTime() {
        return leastAccessTime;
    }

    public void setData(final byte[] data) {
        this.data = data;
    }

    public void setLeastAccessTime(final Instant leastAccessTime) {
        this.leastAccessTime = leastAccessTime;
    }
}
