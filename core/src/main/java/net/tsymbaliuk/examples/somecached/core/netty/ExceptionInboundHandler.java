package net.tsymbaliuk.examples.somecached.core.netty;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import net.tsymbaliuk.examples.somecached.core.model.MemcacheResponseCode;
import net.tsymbaliuk.examples.somecached.core.model.MemcacheResponseMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * ChannelInboundHandlerAdapter to properly handle exceptions thrown by decoder/encoder or service.
 */
public class ExceptionInboundHandler extends ChannelInboundHandlerAdapter {

    private static final Logger logger = LoggerFactory.getLogger(ExceptionInboundHandler.class);

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        logger.info("Exception Handled in ExceptionInboundHandler");
        logger.info(cause.getLocalizedMessage());
        final MemcacheResponseMessage resp = MemcacheResponseMessage.builder()
                .setResponseCode(MemcacheResponseCode.ERROR)
                .setErrorMessage("failed to process request due to exception " + cause)
                .build();
        ctx.channel().writeAndFlush(resp);
        ctx.close();
    }
}
