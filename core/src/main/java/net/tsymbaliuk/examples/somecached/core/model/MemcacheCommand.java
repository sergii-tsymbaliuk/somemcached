package net.tsymbaliuk.examples.somecached.core.model;

import net.tsymbaliuk.examples.somecached.core.netty.MemcacheCommandParsingException;

/**
 * Defines supported memcache text protocol commands.
 */
public enum MemcacheCommand {
    UNKNOWN,
    SET,
    ADD,
    REPLACE,
    DELETE,
    GET;

    /**
     * Converts text representation to MemcacheCommand.
     * @param name
     * @return MemcacheCommand for provided text representation
     * @throws MemcacheCommandParsingException if command is unknown
     */
    public static MemcacheCommand fromString(final String name) throws MemcacheCommandParsingException {
        for (MemcacheCommand c : MemcacheCommand.values()) {
            if (c.name().equalsIgnoreCase(name)) {
                return c;
            }
        }
        throw new MemcacheCommandParsingException("Unknown command name: " + name);
    }
}