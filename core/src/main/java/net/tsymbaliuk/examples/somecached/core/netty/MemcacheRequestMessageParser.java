package net.tsymbaliuk.examples.somecached.core.netty;

import net.tsymbaliuk.examples.somecached.core.model.MemcacheCommand;
import net.tsymbaliuk.examples.somecached.core.model.MemcacheRequestMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Utility class to parse memcache command from string representation.
 */
public class MemcacheRequestMessageParser {

    private static final Logger logger = LoggerFactory
            .getLogger(MemcacheRequestMessageParser.class);

    private static final String NO_REPLY = "noreply";

    /**
     * Parses string with memcache text protocol command, put parsed information to messageBuilder.
     * @param messageBuilder instance of {@link MemcacheRequestMessage} to put decoded information to
     * @param commandLine string containing command part of memcache text message
     * @throws MemcacheCommandParsingException if occurred error during parsing provided command.
     */
    public void parseCommand(
            final MemcacheRequestMessage.MemcacheRequestMessageBuilder messageBuilder,
            final String commandLine) throws MemcacheCommandParsingException {

        logger.info("Start parsing command '{}'", commandLine);

        final String[] parts = commandLine.split("\\s+"); // Protocol supports only spaces
        checkCommandArgumentNumber(messageBuilder, parts, 2, 6);
        final MemcacheCommand memcacheCommand = MemcacheCommand.fromString(parts[0]);
        messageBuilder.setCommand(memcacheCommand);
        if (memcacheCommand == MemcacheCommand.GET) {
            logger.info("Identified GET command");
            for (int i = 1; i < parts.length; i++) {
                messageBuilder.addKey(parts[i]);
            }
        } else {
            messageBuilder.addKey(parts[1]);
            if (memcacheCommand == MemcacheCommand.SET || memcacheCommand == MemcacheCommand.ADD
                    || memcacheCommand == MemcacheCommand.REPLACE) {
                checkCommandArgumentNumber(messageBuilder, parts, 5, 6);
                parseFlags(messageBuilder, parts[2]);
                parseExptime(messageBuilder, parts[3]);
                parseBytes(messageBuilder, parts[4]);
                if (parts.length == 6) {
                    parseNoReply(messageBuilder, parts[5]);
                }
            } else if (memcacheCommand == MemcacheCommand.DELETE) {
                logger.info("Identified DELETE command");
                checkCommandArgumentNumber(messageBuilder, parts, 2, 4);
                if (parts.length == 3) {
                    try {
                        parseExptime(messageBuilder, parts[2]);
                    } catch (MemcacheCommandParsingException e) {
                        parseNoReply(messageBuilder, parts[2]);
                    }
                } else if (parts.length == 4) {
                    parseExptime(messageBuilder, parts[2]);
                    parseNoReply(messageBuilder, parts[3]);
                }
            }
        }
    }

    /** Ensure the string contains minimum required number of parts to parse based on command. */
    void checkCommandArgumentNumber(
            final MemcacheRequestMessage.MemcacheRequestMessageBuilder message,
            final String[] parts, final int min, final int max)
            throws MemcacheCommandParsingException {
        if (parts.length < min) {
            logger.error("Command {} expects from {} to {} arguments, but {} instead were provided",
                    message.getCommand(), min, max, parts.length);
            throw new MemcacheCommandParsingException(String.format(
                    "Command %s expects from %s to %s arguments, but %s instead were provided",
                    message.getCommand(), min, max, parts.length));
        }
    }

    int parseIntWithCheck(final String part, final String argName)
            throws MemcacheCommandParsingException {
        try {
            return Integer.parseInt(part);
        } catch (NumberFormatException e) {
            throw new MemcacheCommandParsingException(
                    "Illegal integer format for " + argName + " argument: " + part, e);
        }
    }

    void parseFlags(final MemcacheRequestMessage.MemcacheRequestMessageBuilder message,
            final String part)
            throws MemcacheCommandParsingException {
        message.setFlags(parseIntWithCheck(part, "flags"));
    }

    void parseExptime(final MemcacheRequestMessage.MemcacheRequestMessageBuilder message,
            final String part)
            throws MemcacheCommandParsingException {
        message.setExpTime(parseIntWithCheck(part, "expiration"));
    }

    void parseBytes(final MemcacheRequestMessage.MemcacheRequestMessageBuilder message,
            final String part)
            throws MemcacheCommandParsingException {
        message.setBytes(parseIntWithCheck(part, "bytes"));
    }

    void parseNoReply(final MemcacheRequestMessage.MemcacheRequestMessageBuilder message,
            final String part)
            throws MemcacheCommandParsingException {
        if (NO_REPLY.equalsIgnoreCase(part)) {
            message.setNoReply(true);
        } else {
            throw new MemcacheCommandParsingException(
                    "Unknown argument '" + part + "', only 'noreply' is supported");
        }
    }
}
