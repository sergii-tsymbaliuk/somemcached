package net.tsymbaliuk.examples.somecached.core.model;

/**
 * Memcache response codes.
 */
public enum MemcacheResponseCode {
    // Is not a command, but a instruction to service to suppress output
    NOREPLY,

    // Value has successfully been stored.
    STORED,

    //The value was not stored, but not because of an error.
    // For commands where you are adding a or updating a value if it exists (such as add and replace),
    // or where the item has already been set to be deleted.
    NOT_STORED,

    // When using a cas command, the item you are trying to store already exists and has been
    // modified since you last checked it.
    EXISTS,

    // The item you are trying to store, update or delete does not exist or has already been deleted.
    NOT_FOUND,

    // You submitted a nonexistent command name.
    ERROR,

    // errorstring There was an error in the input line, the detail is contained in errorstring.
    CLIENT_ERROR,

    // errorstring  There was an error in the server that prevents it from returning the information.
    // In extreme conditions, the server may disconnect the client after this error occurs.
    SERVER_ERROR,

    // The requested key has been found, and the stored key, flags and data block are returned,
    // of the specified length.
    VALUE, // + keys flags length

    // The requested key was deleted from the server.
    DELETED,

    // name value  A line of statistics data.
    STAT, // UNIMPLEMENTED

    // The end of the statistics data.
    END;
}
