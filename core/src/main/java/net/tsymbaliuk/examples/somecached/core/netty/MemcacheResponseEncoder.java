package net.tsymbaliuk.examples.somecached.core.netty;

import net.tsymbaliuk.examples.somecached.core.model.MemcacheKey;
import net.tsymbaliuk.examples.somecached.core.model.MemcacheResponseCode;
import net.tsymbaliuk.examples.somecached.core.model.MemcacheResponseMessage;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

import static net.tsymbaliuk.examples.somecached.core.netty.Config.BYTES_ENCODING_CHARSET;
import static net.tsymbaliuk.examples.somecached.core.netty.Config.CRLF;
import static net.tsymbaliuk.examples.somecached.core.netty.Config.SP;

/**
 * Encodes {@link MemcacheResponseMessage} to {@link ByteBuf} and writes to channel (socket).
 */
public class MemcacheResponseEncoder extends MessageToByteEncoder<MemcacheResponseMessage> {

    private static final Logger logger = LoggerFactory.getLogger(MemcacheResponseEncoder.class);

    @Override
    protected void encode(final ChannelHandlerContext ctx, final MemcacheResponseMessage msg,
            final ByteBuf out) throws Exception {

        logger.info("Start encoding Response Message {}", msg);

        if (msg.getResponseCode() == MemcacheResponseCode.VALUE) {
            logger.info("Response is VALUE, encoding data blocks:");

            // Encoding data blocks if exist
            for (final Map.Entry<MemcacheKey, byte[]> entry : msg.getData().entrySet()) {
                final MemcacheKey key = entry.getKey();
                final byte[] value = entry.getValue();
                // VALUE <key> <flags> <bytes>\r\n
                out.writeBytes(MemcacheResponseCode.VALUE.name().getBytes(BYTES_ENCODING_CHARSET))
                        .writeByte(SP)
                        .writeBytes(key.getKey().getBytes(BYTES_ENCODING_CHARSET))
                        .writeByte(SP)
                        .writeBytes(String.valueOf(key.getFlags()).getBytes(BYTES_ENCODING_CHARSET))
                        .writeByte(SP)
                        .writeBytes(String.valueOf(value.length).getBytes(BYTES_ENCODING_CHARSET))
                        .writeBytes(CRLF);
                // <data block>\r\n
                out.writeBytes(value).writeBytes(CRLF);
            }
            out.writeBytes(MemcacheResponseCode.END.name().getBytes(BYTES_ENCODING_CHARSET));
        } else if (msg.getResponseCode() != MemcacheResponseCode.NOREPLY) {
            logger.info("Encoding response code {}", msg.getResponseCode());
            // Encoding response status and (if exists) error_message
            out.writeBytes(msg.getResponseCode().name().getBytes(BYTES_ENCODING_CHARSET));
            if (msg.hasErrorMessage()) {
                out.writeByte(SP);
                out.writeBytes(msg.getErrorMessage().getBytes(BYTES_ENCODING_CHARSET));
            }
        } else {
            logger.info("NOREPLY is set ON, skipping to write response");
        }
        out.writeBytes(CRLF);
        logger.info("Done");
    }
}
