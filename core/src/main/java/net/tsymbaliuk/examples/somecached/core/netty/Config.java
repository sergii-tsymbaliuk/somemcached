package net.tsymbaliuk.examples.somecached.core.netty;

import java.nio.charset.Charset;

/**
 * Utility class to holding constants.
 */
final class Config {
    static final Charset BYTES_ENCODING_CHARSET = Charset.defaultCharset();

    static final byte LF = 10;
    static final byte CR = 13;
    static final byte SP = 32;
    static final byte[] CRLF = {CR, LF};

    private Config() {
    }
}
