package net.tsymbaliuk.examples.somecached.core.netty;

/** Exception thrown by memcache command parser. */
public class MemcacheCommandParsingException extends Exception {

    private static final long serialVersionUID = 8020137502993344253L;

    public MemcacheCommandParsingException() {
        super();
    }

    public MemcacheCommandParsingException(String message) {
        super(message);
    }

    public MemcacheCommandParsingException(String message, Throwable cause) {
        super(message, cause);
    }

    public MemcacheCommandParsingException(Throwable cause) {
        super(cause);
    }

    protected MemcacheCommandParsingException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
