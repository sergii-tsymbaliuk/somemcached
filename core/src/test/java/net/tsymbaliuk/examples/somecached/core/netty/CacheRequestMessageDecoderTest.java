package net.tsymbaliuk.examples.somecached.core.netty;

import io.netty.buffer.ByteBufAllocator;
import io.netty.buffer.ByteBufUtil;
import io.netty.buffer.PooledByteBufAllocator;
import io.netty.channel.ChannelHandlerContext;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import net.tsymbaliuk.examples.somecached.core.model.MemcacheRequestMessage;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.core.Is.is;

@RunWith(MockitoJUnitRunner.class)
public class CacheRequestMessageDecoderTest {

    private static final ByteBufAllocator BB_ALLOC = PooledByteBufAllocator.DEFAULT;

    @Mock
    private ChannelHandlerContext chc;

    private final MemcacheRequestMessageParser parser = new MemcacheRequestMessageParser();
    private List<Object> out;

    private MemcacheRequestDecoder decoder;

    @Before
    public void setUp() {
        out = new ArrayList<>();
        this.decoder = new MemcacheRequestDecoder(parser);
    }

    @Test
    public void decode_GetCommand() throws Exception {
        decoder.decode(chc, ByteBufUtil.writeAscii(BB_ALLOC, "set xyzkey 0 0 6\r\nabcdef\r\n"),
                out);
        decoder.decode(chc, ByteBufUtil.writeAscii(BB_ALLOC, "abcdef\r\n"), out);
        assertThat(out, hasSize(1));
        final Object obj = out.get(0);
        assertThat(obj.getClass(), is(MemcacheRequestMessage.class));
        final MemcacheRequestMessage message = (MemcacheRequestMessage) obj;
        assertThat(message.getBytes(), is(6));
        assertThat(message.getValue(), is("abcdef".getBytes(Charset.defaultCharset())));
    }

    @Test
    public void decode_GetCommandValueWithControChars() throws Exception {
        decoder.decode(chc, ByteBufUtil.writeAscii(BB_ALLOC, "set xyzkey 0 0 8\r\nabc\r\ndef\r\n"),
                out);
        decoder.decode(chc, ByteBufUtil.writeAscii(BB_ALLOC, "abc\r\ndef\r\n"), out);
        assertThat(out, hasSize(1));
        final Object o = out.get(0);
        assertThat(o.getClass(), is(MemcacheRequestMessage.class));
        final MemcacheRequestMessage message = (MemcacheRequestMessage) o;
        assertThat(message.getBytes(), is(8));
        assertThat(message.getValue(), is("abc\r\ndef".getBytes(Charset.defaultCharset())));
    }
}