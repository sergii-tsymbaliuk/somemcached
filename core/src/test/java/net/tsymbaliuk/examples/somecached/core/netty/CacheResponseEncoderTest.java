package net.tsymbaliuk.examples.somecached.core.netty;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import net.tsymbaliuk.examples.somecached.core.model.MemcacheItem;
import net.tsymbaliuk.examples.somecached.core.model.MemcacheKey;
import net.tsymbaliuk.examples.somecached.core.model.MemcacheResponseCode;
import net.tsymbaliuk.examples.somecached.core.model.MemcacheResponseMessage;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;

import static net.tsymbaliuk.examples.somecached.core.netty.Config.BYTES_ENCODING_CHARSET;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.endsWith;
import static org.hamcrest.Matchers.is;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyByte;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CacheResponseEncoderTest {

    @Mock
    private ChannelHandlerContext ctx;
    @Mock
    private ByteBuf byteBuf;
    @Captor
    private ArgumentCaptor<byte[]> byteBufCaptor;
    private MemcacheResponseEncoder encoder;

    @Before
    public void setUp() throws Exception {
        encoder = new MemcacheResponseEncoder();
        when(byteBuf.writeBytes(any(byte[].class))).thenReturn(byteBuf);
        when(byteBuf.writeByte(any(byte.class))).thenReturn(byteBuf);
    }

    @Test
    public void encode_value() throws Exception {
        byteBuf = Unpooled.buffer();
        final List<MemcacheItem> items = List.of(
                new MemcacheItem(
                        new MemcacheKey("first-data-key-1", 4, 453),
                        "test-data-1".getBytes(BYTES_ENCODING_CHARSET)),
                new MemcacheItem(
                        new MemcacheKey("second-data-key-2", 7, 453432),
                        "test-data-2 \ttada\r\n".getBytes(BYTES_ENCODING_CHARSET)));

        final MemcacheResponseMessage msg = MemcacheResponseMessage.builder()
                .setResponseCode(MemcacheResponseCode.VALUE)
                .addAllValues(items)
                .build();

        encoder.encode(ctx, msg, byteBuf);

        final String output =  byteBuf.readCharSequence(byteBuf.readableBytes(), BYTES_ENCODING_CHARSET).toString();
        assertThat(output, containsString(
                "VALUE first-data-key-1 4 11\r\n"
                        + "test-data-1\r\n"));
        assertThat(output, containsString(
                "VALUE second-data-key-2 7 19\r\n"
                        + "test-data-2 \ttada\r\n\r\n"));
        assertThat(output, endsWith("END\r\n"));
    }

    @Test
    public void encode_stored() throws Exception {
        final MemcacheResponseMessage msg = MemcacheResponseMessage.builder()
                .setResponseCode(MemcacheResponseCode.STORED)
                .build();

        encoder.encode(ctx, msg, byteBuf);

        verify(byteBuf).writeBytes("STORED".getBytes(BYTES_ENCODING_CHARSET));
        verify(byteBuf).writeBytes("\r\n".getBytes(BYTES_ENCODING_CHARSET));
    }

    @Test
    public void encode_error_message() throws Exception {
        final MemcacheResponseMessage msg = MemcacheResponseMessage.builder()
                .setResponseCode(MemcacheResponseCode.SERVER_ERROR)
                .setErrorMessage("Test error message")
                .build();

        encoder.encode(ctx, msg, byteBuf);

        verify(byteBuf).writeBytes("SERVER_ERROR".getBytes(BYTES_ENCODING_CHARSET));
        verify(byteBuf).writeByte(" ".getBytes(BYTES_ENCODING_CHARSET)[0]);
        verify(byteBuf).writeBytes("Test error message".getBytes(BYTES_ENCODING_CHARSET));
        verify(byteBuf).writeBytes("\r\n".getBytes(BYTES_ENCODING_CHARSET));
    }

    @Test
    public void encode_noreply() throws Exception {
        final MemcacheResponseMessage msg = MemcacheResponseMessage.builder()
                .setResponseCode(MemcacheResponseCode.NOREPLY)
                .setErrorMessage("Test error message")
                .build();

        encoder.encode(ctx, msg, byteBuf);

        verify(byteBuf, never()).writeByte(anyByte());
        verify(byteBuf).writeBytes(byteBufCaptor.capture());
        byte[] bytes = byteBufCaptor.getValue();
        assertThat(bytes, is("\r\n".getBytes(BYTES_ENCODING_CHARSET)));
    }
}