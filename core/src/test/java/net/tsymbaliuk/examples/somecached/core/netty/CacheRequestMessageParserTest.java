package net.tsymbaliuk.examples.somecached.core.netty;

import net.tsymbaliuk.examples.somecached.core.model.MemcacheCommand;
import net.tsymbaliuk.examples.somecached.core.model.MemcacheRequestMessage;
import org.hamcrest.core.Is;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsIterableContaining.hasItems;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(MockitoJUnitRunner.class)
public class CacheRequestMessageParserTest {

    private MemcacheRequestMessage.MemcacheRequestMessageBuilder messageBuilder;
    private MemcacheRequestMessageParser parser;

    @Before
    public void setUp() {
        messageBuilder = MemcacheRequestMessage.builder();
        parser = new MemcacheRequestMessageParser();
    }

    @Test
    public void testParseCommand_set() throws MemcacheCommandParsingException {
        parser.parseCommand(messageBuilder, "set  xyzkey  7  4  6\r\n");

        assertThat(messageBuilder.getCommand(), Is.is(MemcacheCommand.SET));
        assertThat(messageBuilder.getKeys(), hasItems("xyzkey"));
        assertThat(messageBuilder.getFlags(), is(7L));
        assertThat(messageBuilder.getExpTime(), is(4L));
        assertThat(messageBuilder.getBytes(), is(6));
        assertFalse(messageBuilder.isNoReply());
    }

    @Test
    public void testParseCommand_set_noreply() throws MemcacheCommandParsingException {
        parser.parseCommand(messageBuilder, "set  abcdef  7  4  6 noreply\r\n");

        assertThat(messageBuilder.getCommand(), Is.is(MemcacheCommand.SET));
        assertThat(messageBuilder.getKeys(), hasItems("abcdef"));
        assertThat(messageBuilder.getFlags(), is(7L));
        assertThat(messageBuilder.getExpTime(), is(4L));
        assertThat(messageBuilder.getBytes(), is(6));
        assertTrue(messageBuilder.isNoReply());
    }

    @Test
    public void testParseCommand_add() throws MemcacheCommandParsingException {
        parser.parseCommand(messageBuilder, "add  xyzkey  7  4  6\r\n");

        assertThat(messageBuilder.getCommand(), Is.is(MemcacheCommand.ADD));
        assertThat(messageBuilder.getKeys(), hasItems("xyzkey"));
        assertThat(messageBuilder.getFlags(), is(7L));
        assertThat(messageBuilder.getExpTime(), is(4L));
        assertThat(messageBuilder.getBytes(), is(6));
        assertFalse(messageBuilder.isNoReply());
    }

    @Test
    public void testParseCommand_replace_noreply() throws MemcacheCommandParsingException {
        parser.parseCommand(messageBuilder, "replace  xyzkey  7  4  6 noreply\r\n");

        assertThat(messageBuilder.getCommand(), Is.is(MemcacheCommand.REPLACE));
        assertThat(messageBuilder.getKeys(), hasItems("xyzkey"));
        assertThat(messageBuilder.getFlags(), is(7L));
        assertThat(messageBuilder.getExpTime(), is(4L));
        assertThat(messageBuilder.getBytes(), is(6));
        assertTrue(messageBuilder.isNoReply());
    }

    @Test
    public void testParseCommand_get() throws MemcacheCommandParsingException {
        parser.parseCommand(messageBuilder, "get  xyz435key secondKey thirdKey");

        assertThat(messageBuilder.getCommand(), Is.is(MemcacheCommand.GET));
        assertThat(messageBuilder.getKeys(), hasItems("xyz435key", "secondKey", "thirdKey"));
        assertThat(messageBuilder.getKeys().size(), is(3));
        assertThat(messageBuilder.getFlags(), is(0L));
        assertThat(messageBuilder.getExpTime(), is(0L));
        assertFalse(messageBuilder.isNoReply());
    }

    @Test
    public void testParseCommand_get_oneKey() throws MemcacheCommandParsingException {
        parser.parseCommand(messageBuilder, "get  xyz435key\r\n");

        assertThat(messageBuilder.getCommand(), Is.is(MemcacheCommand.GET));
        assertThat(messageBuilder.getKeys(), hasItems("xyz435key"));
        assertThat(messageBuilder.getKeys().size(), is(1));
        assertThat(messageBuilder.getFlags(), is(0L));
        assertThat(messageBuilder.getExpTime(), is(0L));
        assertFalse(messageBuilder.isNoReply());
    }

    @Test
    public void testParseCommand_delete() throws MemcacheCommandParsingException {
        parser.parseCommand(messageBuilder, "delete xyz435key");
        assertThat(messageBuilder.getCommand(), Is.is(MemcacheCommand.DELETE));
        assertThat(messageBuilder.getKeys(), hasItems("xyz435key"));
        assertThat(messageBuilder.getKeys().size(), is(1));
        assertThat(messageBuilder.getFlags(), is(0L));
        assertThat(messageBuilder.getExpTime(), is(0L));
        assertFalse(messageBuilder.isNoReply());
    }

    @Test
    public void testParseCommand_delete_noreply() throws MemcacheCommandParsingException {
        parser.parseCommand(messageBuilder, "delete xyz435key noreply");
        assertThat(messageBuilder.getCommand(), Is.is(MemcacheCommand.DELETE));
        assertThat(messageBuilder.getKeys(), hasItems("xyz435key"));
        assertThat(messageBuilder.getKeys().size(), is(1));
        assertThat(messageBuilder.getExpTime(), is(0L));
        assertTrue(messageBuilder.isNoReply());
    }

    @Test
    public void testParseCommand_delete_exptime() throws MemcacheCommandParsingException {
        parser.parseCommand(messageBuilder, "delete xyz435key 30");
        assertThat(messageBuilder.getCommand(), Is.is(MemcacheCommand.DELETE));
        assertThat(messageBuilder.getKeys(), hasItems("xyz435key"));
        assertThat(messageBuilder.getKeys().size(), is(1));
        assertThat(messageBuilder.getExpTime(), is(30L));
        assertFalse(messageBuilder.isNoReply());
    }

    @Test
    public void testParseCommand_delete_exptime_noreply() throws MemcacheCommandParsingException {
        parser.parseCommand(messageBuilder, "delete xyz435key 30 noreply");
        assertThat(messageBuilder.getCommand(), Is.is(MemcacheCommand.DELETE));
        assertThat(messageBuilder.getKeys(), hasItems("xyz435key"));
        assertThat(messageBuilder.getKeys().size(), is(1));
        assertThat(messageBuilder.getExpTime(), is(30L));
        assertTrue(messageBuilder.isNoReply());
    }

    @Test(expected = MemcacheCommandParsingException.class)
    public void testParseCommand_bad_command() throws MemcacheCommandParsingException {
        parser.parseCommand(messageBuilder, "bad  xyzkey  7  4  6 noreply\r\n");

        assertThat(messageBuilder.getCommand(), Is.is(MemcacheCommand.UNKNOWN));
        assertThat(messageBuilder.getKeys(), hasItems("xyzkey"));
        assertThat(messageBuilder.getFlags(), is(7));
        assertThat(messageBuilder.getExpTime(), is(4L));
        assertThat(messageBuilder.getBytes(), is(6));
        assertTrue(messageBuilder.isNoReply());
    }
}